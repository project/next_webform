import * as React from 'react';
import { PropsType } from '../types';

export type messagePropTypes = {
  children: React.ReactNode;
  type: 'error' | 'warning' | 'status' | 'success';
  wrapperProps?: PropsType;
};
const Message = ({ children, type, wrapperProps = {} }: messagePropTypes) => {
  return <div {...wrapperProps}>{children}</div>;
};

export default Message;
